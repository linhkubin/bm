﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SetText : MonoBehaviour
{
    public SetAddress[] list_text;
    // Start is called before the first frame update
    private void Awake()
    {
        for (int i = 0; i < list_text.Length; i++)
        {
            list_text[i].SetNumber(i + 1);
        }
    }
}
