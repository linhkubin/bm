﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SetAddress : MonoBehaviour
{
    public Text txt_address;

    public int address;

    public ESpawnPoints[] listSpawnPoint;

    public void SetNumber(int stt)
    {
        address = stt;
        txt_address.text = address.ToString();
        for (int i = 0; i < listSpawnPoint.Length; i++)
        {
            listSpawnPoint[i].address = address * 100 + i;
            Mapeditor.Instance.list_allSpawnPoints.Add(listSpawnPoint[i]);
        }
    }
}
