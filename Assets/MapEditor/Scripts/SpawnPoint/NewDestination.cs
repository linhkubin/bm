﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NewDestination : MonoBehaviour
{
    public Text number;

    public Vector3 GetPosition()
    {
        return transform.position;
    }

    public void SetPosition(Vector3 pos)
    {
        transform.position = pos; 
    }

    public void SetNumber(int number)
    {
        this.number.text = number.ToString();
    }
}
