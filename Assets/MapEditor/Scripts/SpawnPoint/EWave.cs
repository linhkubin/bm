﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[Serializable]
public class EWave : MonoBehaviour
{
    public int numberWave;
    // list enemy
    public List<EEnemySpawn> list_enemySpawn;
    // number weapon can drop
    public int numberWeapon;
    // spawn in stt
    public bool spawnInLine;
    // delta time to next
    // == 0 if all enemy death
    // != 0 if have time
    public float deltaTimeNext;

    public bool defeatAllToNextWave;

    public List<NewDestination> list_destination;

    public Vector2 followFormation;
    #region UI
    [Header("UI")]
    public Text txt_numberWave;

    public Image imageBG;
    #endregion

    #region notice
    [Header("notice")]
    public GameObject spawnNow;
    public GameObject haveWeapon;
    public GameObject haveLines;

    #endregion

    private void Start()
    {
        GetComponent<Button>().onClick.AddListener(OnClickWave);
    }

    public void SetNumberWave(int numberWave)
    {
        this.numberWave = numberWave;
        txt_numberWave.text = numberWave.ToString();
    }

    public void SetWave( int numberWeapon, bool spawnInLine, float deltaTime)
    {
        this.numberWeapon = numberWeapon;
        this.spawnInLine = spawnInLine;
        this.deltaTimeNext = deltaTime;
    }

    public void SetEnemy(EEnemySpawn newEnemy)
    {
        list_enemySpawn.Add(newEnemy);
    }

    public void OnClickWave()
    {
       

        if (EditorManager.Instance.currentWave != null)
        {
            if (EditorManager.Instance.currentWave.list_enemySpawn.Count <=0 )
            {
                //Destroy(EditorManager.Instance.currentWave.gameObject);
            }
            else
            {
                EditorManager.Instance.currentWave.imageBG.color = Color.white;
                EUImanager.Instance.SaveWave();
            }
        }
        EditorManager.Instance.currentWave = GetComponent<EWave>();
        EUImanager.Instance.OpenWavePanel();
        imageBG.color = Color.grey;

        //hien enemy
        for (int i = 0; i < EditorManager.Instance.currentWave.list_enemySpawn.Count; i++)
        {
            EditorManager.Instance.currentWave.list_enemySpawn[i].gameObject.SetActive(true);
        }

        //hien line
        for (int i = 0; i < EditorManager.Instance.currentWave.list_destination.Count; i++)
        {
            EditorManager.Instance.currentWave.list_destination[i].gameObject.SetActive(true);
        }

    }

    public void Initialize()
    {

    }

}