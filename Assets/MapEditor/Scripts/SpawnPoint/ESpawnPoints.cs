﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//[Serializable]
public class ESpawnPoints : MonoBehaviour
{
    //stt in Editor
    public int address;
    //public Text txt_address;

    // list wave
    public List<EWave> list_wave;
    // position point
    public Vector2 positionSpawnPoint;


    /*
     -2 default
     -1 xoaspawn point
     -10 replace spawn point
     0 spawn point
     1 barier
     11 xoa barier
     2 end point
         
         */

    public int thisType = -2;

    public void SetPosition(Vector2 pos)
    {
        positionSpawnPoint = pos;
    }

    public Vector2 GetPosition()
    {
        return transform.position;
    }

    public int GetAdress()
    {
        return address;
    }

    public void SetWave(EWave wave)
    {
        //Debug.Log(wave);
        list_wave.Add(wave);
        wave.SetNumberWave(list_wave.Count);
    }

    // Start is called before the first frame update
    void Start()
    {
        GetComponent<Button>().onClick.AddListener(CreateSpawnPointButton);
    }

    public void CreateSpawnPointButton()
    {
        // spawn point
        if (EditorManager.Instance.buttonChoice == 0)
        {
            GetComponent<Image>().color = Color.red;

            InitSpawnPoint(EditorManager.Instance.CheckPoint(address, GetComponent<ESpawnPoints>()));

            thisType = 0;
        }


        //xoa spawn point
        if (EditorManager.Instance.buttonChoice == -1 && thisType == 0)
        {
            Default();

            EditorManager.Instance.currentSpawnPoints = (EditorManager.Instance.CheckPoint(address, GetComponent<ESpawnPoints>()));

            EditorManager.Instance.list_spawnPoints.Remove(EditorManager.Instance.currentSpawnPoints);

            thisType = -2;
        }

        //replace spawn point
        if (EditorManager.Instance.buttonChoice == -10)
        {
            GetComponent<Image>().color = Color.red;

            thisType = 0;

            list_wave = EditorManager.Instance.currentSpawnPoints.list_wave;

            EditorManager.Instance.currentSpawnPoints.Clear();

            InitSpawnPoint(GetComponent<ESpawnPoints>());

            EditorManager.Instance.buttonChoice = 0;

            EditorManager.Instance.list_spawnPoints.Add(GetComponent<ESpawnPoints>());
        }

        //barrier
        if (EditorManager.Instance.buttonChoice == 1)
        {
            if (!EditorManager.Instance.list_barrier.Contains(GetComponent<ESpawnPoints>()))
            {
                GetComponent<Image>().color = new Color32(180, 111, 49, 255);
                EditorManager.Instance.list_barrier.Add(GetComponent<ESpawnPoints>());
                thisType = 1;
            }

        }
        // remove barrier
        if (EditorManager.Instance.buttonChoice == 11)
        {
            if (EditorManager.Instance.list_barrier.Contains(GetComponent<ESpawnPoints>()))
            {
                EditorManager.Instance.list_barrier.Remove(GetComponent<ESpawnPoints>());
                Clear();
                thisType = -2;
            }

        }

        //end point
        if (EditorManager.Instance.buttonChoice == 2)
        {
            GetComponent<Image>().color = Color.black;

            if (EditorManager.Instance.stopPoint != null)
            {
                EditorManager.Instance.stopPoint.Clear();
            }

            EditorManager.Instance.stopPoint = GetComponent<ESpawnPoints>();

            thisType = 2;
        }
        //Debug.Log(address + "_" + transform.position);


        //line
        if (EditorManager.Instance.buttonChoice == 3)
        {
            GameObject go = Instantiate(EUImanager.Instance.prefab_destination,EUImanager.Instance.world);

            EditorManager.Instance.currentWave.list_destination.Add(go.GetComponent<NewDestination>());
            go.GetComponent<NewDestination>().SetNumber(EditorManager.Instance.currentWave.list_destination.Count - 1);
            go.GetComponent<NewDestination>().SetPosition(transform.position);
        }

        //enemy
        if (EditorManager.Instance.buttonChoice == 4)
        {
            GameObject go = Instantiate(EditorManager.Instance.eEnemyPrefab);
            go.GetComponent<EEnemySpawn>().SetResource(EditorManager.Instance.indexId, EditorManager.Instance.indexType, EditorManager.Instance.GetSpriteEnemy(EditorManager.Instance.indexId, EditorManager.Instance.indexType));
            go.GetComponent<EEnemySpawn>().SetEnemyPosition(transform.position);
            EditorManager.Instance.currentWave.SetEnemy(go.GetComponent<EEnemySpawn>());
            go.name = "point:" + (EditorManager.Instance.list_spawnPoints.IndexOf(EditorManager.Instance.currentSpawnPoints) + 1) + "_wave:" + (EditorManager.Instance.currentSpawnPoints.list_wave.IndexOf(EditorManager.Instance.currentWave) + 1) + "_enemy:" + (EditorManager.Instance.currentWave.list_enemySpawn.Count);
        }
    }



    public void InitSpawnPoint(ESpawnPoints spoint)
    {

        EUImanager.Instance.SavePoint();

        if (spoint == null)
        {
            spoint = GetComponent<ESpawnPoints>();
        }
        else
        {
            //khoi tao
            //    for (int i = 0; i < spoint.list_wave.Count; i++)
            //    {

            //    }
        }

        EditorManager.Instance.currentSpawnPoints = spoint;

        for (int i = 0; i < EditorManager.Instance.currentSpawnPoints.list_wave.Count; i++)
        {
            EditorManager.Instance.currentSpawnPoints.list_wave[i].gameObject.SetActive(true);
        }

        EUImanager.Instance.OpenSpawnPointPanel();

        EUImanager.Instance.panel_editor.SetActive(false);

    }

    public void Clear()
    {
        Default();
        EditorManager.Instance.RemovePoint(address);
    }

    public void Default()
    {
        GetComponent<Image>().color = new Color32(182, 215, 168, 255);
        thisType = -2;
    }

    public void WhatIs(int type)
    {
        if (type == -1)
        {
            //default
            Clear();
        }
        else if (type == 0)
        {
            //sp
            EditorManager.Instance.buttonChoice = 0;
            CreateSpawnPointButton();
        }
        else if (type == 1)
        {
            //barrier
            EditorManager.Instance.buttonChoice = 1;
            CreateSpawnPointButton();
        }
        else if (type == 2)
        {
            //end point
            EditorManager.Instance.buttonChoice = 2;
            CreateSpawnPointButton();
        }

    }

    public void Test()
    {
        GetComponent<Image>().color = Color.yellow;

    }
}

