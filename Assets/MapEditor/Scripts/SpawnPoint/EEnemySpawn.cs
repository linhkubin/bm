﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//[Serializable]
public class EEnemySpawn : MonoBehaviour
{
    // position spawn
    public Vector2 positionEnemy;
    // type enemy
    public int id;

    public int type;

    public Image image;

    public SpriteRenderer spriteImage;

    public Text sttSpawn;

    public void SetEnemyPosition(Vector2 pos)
    {
        transform.position = pos;
        //positionEnemy = pos;
    }

    public Vector2 GetPosition()
    {
        return transform.position;
    }


    public int GetIntID()
    {
        return id;
    }

    public int GetIntType()
    {
        return type;
    }


    public void SetResource(int id, int type,Sprite sprite)
    {
        this.id = id;
        this.type = type;
        image.sprite = sprite;
        spriteImage.sprite = sprite;
    }

    private void Start()
    {
        GetComponent<Button>().onClick.AddListener(Click);
    }

    public void Click()
    {
        EUImanager.Instance.CloseSpawnPointPanel();
        EditorManager.Instance.SetIndexEnemy(id, type);

        EUImanager.Instance.panel_newEnemy.SetActive(true);

        EditorManager.Instance.buttonChoice = 4;
    }

    public void SetInCurrentWave()
    {
        EditorManager.Instance.currentWave.SetEnemy(GetComponent<EEnemySpawn>());
        gameObject.name = "point:" + (EditorManager.Instance.list_spawnPoints.IndexOf(EditorManager.Instance.currentSpawnPoints) + 1) + "_wave:" + (EditorManager.Instance.currentSpawnPoints.list_wave.IndexOf(EditorManager.Instance.currentWave) + 1) + "_enemy:" + (EditorManager.Instance.currentWave.list_enemySpawn.Count);
        gameObject.SetActive(false);
    }
}
