﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class EnemySprite 
{
    public int id;
    public Sprite sprite;
    public GameObject go;
}
