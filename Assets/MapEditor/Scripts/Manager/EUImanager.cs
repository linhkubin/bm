﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EUImanager : MonoBehaviour
{
    private static EUImanager instance;
    public static EUImanager Instance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<EUImanager>();
            }
            return instance;
        }
    }


    public List<EnemySprite> list_enemySprite;

    public List<EnemySprite> list_BossSprite;

    public List<GameObject> list_address;

    #region spawn point
    [Header("spawn point")]
    public GameObject g_spawnPoint;

    public Transform tf_wave;

    public Transform tf_enemyMove;

    public Transform tf_boss;

    public Transform tf_mix;

    public Transform tf_enemyTarget;

    #endregion

    #region Prefabs
    [Header("Prefabs")]
    public GameObject prefab_destination;

    public GameObject prefab_spawnPoint;

    public GameObject prefab_wave;

    public GameObject prefab_enemy;

    public GameObject prefab_boss;

    #endregion

    #region Wave
    [Header("Wave")]

    public Text txt_numberEnemy;

    public InputField inp_timeNextWave;

    public Text txt_timeNextWave;

    public InputField inp_numberWeapon;

    public Text txt_numberWeapon;

    public Image img_standInLine;

    public Image img_defeatAll;

    public Image img_lines;

    #endregion

    #region Enemy
    [Header("Enemy")]


    public Transform tfContent;

    private Vector3 initTfContent;

    #endregion

    #region Panel
    public GameObject panel_editor;

    public GameObject panel_point;

    public GameObject panel_wave;

    public GameObject panel_enemy;

    public GameObject panel_destination;

    public GameObject panel_newEnemy;

    #endregion

    #region tranform world

    public Transform world;

    #endregion
    // Start is called before the first frame update
    void Start()
    {
        //index tf content
        initTfContent = tfContent.GetComponent<RectTransform>().localPosition;

        //bonus
        for (int i = 0; i < SpriteManager.Instance.list_bonus.Length; i++)
        {
            GameObject go = Instantiate(prefab_enemy, tf_mix);
            go.GetComponent<EEnemySpawn>().SetResource(SpriteManager.Instance.list_bonus[i].id, Constant.TYPE_ENEMY_BONUS, SpriteManager.Instance.list_bonus[i].sprite);
        }

        //booster
        for (int i = 0; i < SpriteManager.Instance.list_booster.Length; i++)
        {
            GameObject go = Instantiate(prefab_enemy, tf_mix, false);
            go.GetComponent<EEnemySpawn>().SetResource(SpriteManager.Instance.list_booster[i].id, Constant.TYPE_ENEMY_BOOSTER, SpriteManager.Instance.list_booster[i].sprite);
        }

        //gold
        GameObject go_mix = Instantiate(prefab_enemy, tf_mix, false);
        go_mix.GetComponent<EEnemySpawn>().SetResource(SpriteManager.Instance.gold.id, Constant.TYPE_ENEMY_GOLD, SpriteManager.Instance.gold.sprite);

        //enemy Target
        for (int i = 0; i < SpriteManager.Instance.list_enemyTarget.Length; i++)
        {
            GameObject go = Instantiate(prefab_enemy, tf_enemyTarget, false);
            go.GetComponent<EEnemySpawn>().SetResource(SpriteManager.Instance.list_enemyTarget[i].id, Constant.TYPE_ENEMY_TARGET, SpriteManager.Instance.list_enemyTarget[i].sprite);
        }

        //enemy Move
        for (int i = 0; i <SpriteManager.Instance.list_enemyMove.Length; i++)
        {
            GameObject go = Instantiate(prefab_enemy, tf_enemyMove, false);
            go.GetComponent<EEnemySpawn>().SetResource(SpriteManager.Instance.list_enemyMove[i].id, Constant.TYPE_ENEMY_MOVE, SpriteManager.Instance.list_enemyMove[i].sprite);
        }

        //boss
        for (int i = 0; i < SpriteManager.Instance.list_boss.Length; i++)
        {
            GameObject go = Instantiate(prefab_enemy);
            go.transform.SetParent(tf_boss, false);
            go.GetComponent<EEnemySpawn>().SetResource(SpriteManager.Instance.list_boss[i].id, Constant.TYPE_ENEMY_BOSS, SpriteManager.Instance.list_boss[i].sprite);
        }

    }

    public void ResetTranform()
    {
        tfContent.GetComponent<RectTransform>().localPosition = initTfContent;
    }


    public void OpenSpawnPointPanel()
    {
        g_spawnPoint.SetActive(true);
    }

    public void CloseSpawnPointPanel()
    {
        g_spawnPoint.SetActive(false);
    }

    public void NewWave()
    {
        GameObject go = Instantiate(prefab_wave);
        go.transform.SetParent(tf_wave, false);
        //EditorManager.Instance.currentWave = go.GetComponent<EWave>();
        EditorManager.Instance.currentSpawnPoints.SetWave(go.GetComponent<EWave>());

        go.GetComponent<EWave>().OnClickWave();
    }

    public void DeleteWave()
    {

    }

    public void NewEnemy()
    {

    }

    public void SpawnInline()
    {
        EditorManager.Instance.currentWave.spawnInLine = !EditorManager.Instance.currentWave.spawnInLine;
        img_standInLine.color = EditorManager.Instance.currentWave.spawnInLine ? Color.red : Color.white;
    }

    public void DefeatAllToNext()
    {
        EditorManager.Instance.currentWave.defeatAllToNextWave = !EditorManager.Instance.currentWave.defeatAllToNextWave;
        img_defeatAll.color = EditorManager.Instance.currentWave.defeatAllToNextWave ? Color.red : Color.white;
    }


    //lay tu text
    //set vao input field
    public void ResetWaveData()//linh
    {
        //number enemy
        txt_numberEnemy.text = EditorManager.Instance.currentWave.list_enemySpawn.Count.ToString();
        //time next
        inp_timeNextWave.text = EditorManager.Instance.currentWave.deltaTimeNext.ToString();
        //number weapon
        inp_numberWeapon.text = EditorManager.Instance.currentWave.numberWeapon.ToString();
        //stand in line
        img_standInLine.color = EditorManager.Instance.currentWave.spawnInLine ? Color.red : Color.white;

        img_defeatAll.color = EditorManager.Instance.currentWave.defeatAllToNextWave ? Color.red : Color.white;

        CheckListLine();

    }

    public void SaveWave()
    {
        EditorManager.Instance.currentWave.spawnNow.SetActive(false);
        EditorManager.Instance.currentWave.haveWeapon.SetActive(false);
        EditorManager.Instance.currentWave.haveLines.SetActive(false);

        //number weapon
        EditorManager.Instance.currentWave.numberWeapon = int.Parse(txt_numberWeapon.text);

        if (EditorManager.Instance.currentWave.numberWeapon > 0)
        {
            EditorManager.Instance.currentWave.haveWeapon.SetActive(true);
        }
        //spawn in line

        // delta time spawn
        EditorManager.Instance.currentWave.deltaTimeNext = float.Parse(txt_timeNextWave.text);

        if (EditorManager.Instance.currentWave.deltaTimeNext == 0)
        {
            EditorManager.Instance.currentWave.spawnNow.SetActive(true);
        }

        //enemy
        for (int i = 0; i < EditorManager.Instance.currentWave.list_enemySpawn.Count; i++)
        {
            EditorManager.Instance.currentWave.list_enemySpawn[i].gameObject.SetActive(false);
        }


        //line
        for (int i = 0; i < EditorManager.Instance.currentWave.list_destination.Count; i++)
        {
            EditorManager.Instance.currentWave.list_destination[i].gameObject.SetActive(false);
            EditorManager.Instance.currentWave.haveLines.SetActive(true);
        }



        CloseWavePanel();
    }

    public void CloseWavePanel()
    {
        panel_wave.SetActive(false);
    }

    public void OpenWavePanel()
    {
        panel_wave.SetActive(true);
        ResetWaveData();
    }

    public void OpenEnemyPanel()
    {
        panel_enemy.SetActive(true);
    }

    public void CloseEnemyPanel()
    {
        panel_enemy.SetActive(false);
    }

    public void SavePoint()
    {
        if (EditorManager.Instance.currentSpawnPoints != null)
        {
            if (EditorManager.Instance.currentSpawnPoints.list_wave.Count == 0)
            {
                EditorManager.Instance.currentSpawnPoints.Clear();
            }
            else
            {
                SaveWave();

                for (int i = 0; i < EditorManager.Instance.currentSpawnPoints.list_wave.Count; i++)
                {
                    EditorManager.Instance.currentSpawnPoints.list_wave[i].gameObject.SetActive(false);
                }
            }

            CloseSpawnPointPanel();
        }

        EUImanager.Instance.panel_editor.SetActive(true);


    }

    public void GetChoiceButton(int number)
    {
        EditorManager.Instance.buttonChoice = number;
    }

    public GameObject text_preb;
    #region destination
    public void NewDestinationInWave()
    {
        panel_editor.SetActive(false);
        panel_point.SetActive(false);
        panel_destination.SetActive(true);
        EditorManager.Instance.buttonChoice = 3;

        for (int i = 0; i < EditorManager.Instance.currentWave.list_destination.Count; i++)
        {
            EditorManager.Instance.currentWave.list_destination[i].gameObject.SetActive(true);
        }
    }

    public void ClearLines()
    {
        for (int i = 0; i < EditorManager.Instance.currentWave.list_destination.Count; i++)
        {
            Destroy( EditorManager.Instance.currentWave.list_destination[i].gameObject);
        }

        EditorManager.Instance.currentWave.list_destination.Clear();
    }

    public void SaveLines()
    {
        panel_editor.SetActive(true);
        panel_point.SetActive(true);
        panel_destination.SetActive(false);
        EditorManager.Instance.buttonChoice = 0;

        for (int i = 0; i < EditorManager.Instance.currentWave.list_destination.Count; i++)
        {
            EditorManager.Instance.currentWave.list_destination[i].gameObject.SetActive(false);
        }

        CheckListLine();
    }

    private void CheckListLine()
    {
        if (EditorManager.Instance.currentWave.list_destination.Count > 0)
        {
            img_lines.color = Color.red;
        }
        else
        {
            img_lines.color = Color.white;
        }
    }
    #endregion

    #region enemy panel
    public void SaveEnemy()
    {
        panel_newEnemy.SetActive(false);
        OpenSpawnPointPanel();
        EditorManager.Instance.buttonChoice = 0;
    }

    public void ClearEnemy()
    {
        for (int i = 0; i < EditorManager.Instance.currentWave.list_enemySpawn.Count; i++)
        {
            Destroy(EditorManager.Instance.currentWave.list_enemySpawn[i].gameObject);
        }
        EditorManager.Instance.currentWave.list_enemySpawn.Clear();
    }
    #endregion
}


