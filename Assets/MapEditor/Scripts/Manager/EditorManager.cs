﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EditorManager : MonoBehaviour
{
    private static EditorManager instance;
    public static EditorManager Instance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<EditorManager>();
            }
            return instance;
        }
    }
    // 0 = spawn point
    // 1 = barier
    // 2 = end point
    // 11 = remove barrier
    public int buttonChoice;

    #region list need to convert Json

    public ESpawnPoints stopPoint;

    public List<ESpawnPoints> list_barrier; 

    public List<ESpawnPoints> list_spawnPoints;

    public static int LEVEL = 0;

    #endregion

    public ESpawnPoints currentSpawnPoints;

    public EWave currentWave;

    public int currentLevel;

    [Header("prefabs")]
    public GameObject eEnemyPrefab;

    [Header("enemy")]

    public int indexId;
    public int indexType;

    public Sprite GetSpriteEnemy(int indexId, int indexType)
    {
        //bonus
        if (indexType == Constant.TYPE_ENEMY_BONUS)
        {
            return SpriteManager.Instance.list_bonus[indexId].sprite;
        }

        //booster
        if (indexType == Constant.TYPE_ENEMY_BOOSTER)
        {
            return SpriteManager.Instance.list_booster[indexId].sprite;
        }

        //gold
        if (indexType == Constant.TYPE_ENEMY_GOLD)
        {
            return SpriteManager.Instance.gold.sprite;
        }

        //Enemy target
        if (indexType == Constant.TYPE_ENEMY_TARGET  )
        {
            return SpriteManager.Instance.list_enemyTarget[indexId].sprite;
        }

        if (indexType == Constant.TYPE_ENEMY_MOVE || indexType == Constant.TYPE_ENEMY_INLINE)
        {
            return SpriteManager.Instance.list_enemyMove[indexId].sprite;
        }
        //boss
        if (indexType == Constant.TYPE_ENEMY_BOSS)
        {
            return SpriteManager.Instance.list_boss[indexId].sprite;
        }

        return null;
    }

    public void SetIndexEnemy(int id,int type)
    {
        indexId = id;
        indexType = type;
    }

    //--------------------------------------------------------------

    private void Start()
    {
        ReadJson();
    }

    public ESpawnPoints CheckPoint(int address, ESpawnPoints spoint)
    {
        for (int i = 0; i < list_spawnPoints.Count; i++)
        {
            if (list_spawnPoints[i].address == address)
            {
                return list_spawnPoints[i];
            }
        }

        list_spawnPoints.Add(spoint);

        return null;
    }

    public void RemovePoint(int address)
    {
        for (int i = 0; i < list_spawnPoints.Count; i++)
        {
            if (list_spawnPoints[i].address == address)
            {
                list_spawnPoints.RemoveAt(i);
            }
        }
    }

    #region Write Json    

    public MapJson mapManager = new MapJson();

    public void WriteToJson()
    {
        EUImanager.Instance.ResetTranform();

        if (!CheckSaveNotError())
        {
            Debug.Log("CHECK ERROR");
            return;
        }

        mapManager.numberlevel = LEVEL;

        mapManager.rewardCoin = Data.Coin_Reward(LEVEL);

        mapManager.bonusCoin = Data.Coin_Bonus(LEVEL);

        mapManager.stopPoint = stopPoint.GetPosition();

        mapManager.addressStopPoint = stopPoint.GetAdress();

        List<SpawnPointJson> list_tempSPJ = new List<SpawnPointJson>();


        //spawn point
        for (int i = 0; i < list_spawnPoints.Count; i++)
        {

            // list wave
            List<WaveJson> list_tempWJ = new List<WaveJson>();


            for (int j = 0; j < list_spawnPoints[i].list_wave.Count; j++)
            {
                //list enemy
                List<EnemyJson> list_tempEJ = new List<EnemyJson>();

                for (int k = 0; k < list_spawnPoints[i].list_wave[j].list_enemySpawn.Count; k++)
                {
                    list_spawnPoints[i].list_wave[j].list_enemySpawn[k].gameObject.SetActive(true);
                    EnemyJson tempEJ = new EnemyJson(list_spawnPoints[i].list_wave[j].list_enemySpawn[k].GetPosition(), list_spawnPoints[i].list_wave[j].list_enemySpawn[k].GetIntID(), list_spawnPoints[i].list_wave[j].list_enemySpawn[k].GetIntType());

                    list_tempEJ.Add(tempEJ);
                }

                // list line
                List<Vector2> list_line = new List<Vector2>();
                for (int des = 0; des < list_spawnPoints[i].list_wave[j].list_destination.Count; des++)
                {
                    list_line.Add(list_spawnPoints[i].list_wave[j].list_destination[des].GetPosition());
                }

                WaveJson tempWJ = new WaveJson(list_spawnPoints[i].list_wave[j].numberWeapon, list_spawnPoints[i].list_wave[j].spawnInLine, list_spawnPoints[i].list_wave[j].deltaTimeNext, list_tempEJ, list_spawnPoints[i].list_wave[j].defeatAllToNextWave, list_line, list_spawnPoints[i].list_wave[j].followFormation);
                
                list_tempWJ.Add(tempWJ);
            }

            SpawnPointJson tempSPJ = new SpawnPointJson(list_spawnPoints[i].GetPosition(), list_tempWJ);

            tempSPJ.address = list_spawnPoints[i].GetAdress();
            list_tempSPJ.Add(tempSPJ);
        }

        mapManager.SetListSpawnPointJson(list_tempSPJ);


        string json = JsonUtility.ToJson(mapManager, true);


        File.WriteAllText(Application.dataPath + "/GamePlay/Resources/" + LEVEL.ToString() + ".json", json);

        Debug.Log("CONVERT JSON OK !!!");

        SceneManager.LoadScene("EditLevelScene");

        //UIEManager.Instance.OpenIntroPanel("complete save");
    }

    public bool CheckSaveNotError()
    {
        if (list_spawnPoints.Count == 0)
        {
            return false;
        }

        if (stopPoint == null)
        {
            return false;
        }

        return true;    
    }

    #endregion

    #region ReadJson

    public void ReadJson()
    {
        if (!File.Exists(Application.dataPath + "/GamePlay/Resources/" + LEVEL.ToString() + ".json"))
        {
            Debug.Log("null");

            DefaultValue();

            return;
        }

        TextAsset assets = Resources.Load(LEVEL.ToString()) as TextAsset;

        Debug.Log(assets);

        mapManager = JsonUtility.FromJson<MapJson>(assets.text);
        #region xu ly

        //stop point
        Mapeditor.Instance.GetSpawnPoint(mapManager.addressStopPoint).WhatIs(2);


        //spawn point
        for (int i = 0; i < mapManager.spawnPointJsons.Count; i++)
        {
            Mapeditor.Instance.GetSpawnPoint(mapManager.spawnPointJsons[i].address).WhatIs(0);
            for (int j = 0; j < mapManager.spawnPointJsons[i].waveJsons.Count; j++)
            {
                EUImanager.Instance.NewWave();

                //
                //time next
                currentWave.deltaTimeNext = mapManager.spawnPointJsons[i].waveJsons[j].timeToNext;
                EUImanager.Instance.inp_timeNextWave.text = currentWave.deltaTimeNext.ToString();
                //number weapon
                currentWave.numberWeapon = mapManager.spawnPointJsons[i].waveJsons[j].numberWeapon;
                EUImanager.Instance.inp_numberWeapon.text = currentWave.numberWeapon.ToString();
                //stand in line
                currentWave.spawnInLine = mapManager.spawnPointJsons[i].waveJsons[j].spawnInLine;
                //stand in line
                currentWave.defeatAllToNextWave = mapManager.spawnPointJsons[i].waveJsons[j].destroyAllToNext;
                //

                //enemy
                for (int k = 0; k < mapManager.spawnPointJsons[i].waveJsons[j].enemyJsons.Count; k++)
                {
                    EEnemySpawn enemy = Instantiate(eEnemyPrefab).GetComponent<EEnemySpawn>();
                    enemy.SetResource(mapManager.spawnPointJsons[i].waveJsons[j].enemyJsons[k].id, mapManager.spawnPointJsons[i].waveJsons[j].enemyJsons[k].type, GetSpriteEnemy(mapManager.spawnPointJsons[i].waveJsons[j].enemyJsons[k].id, mapManager.spawnPointJsons[i].waveJsons[j].enemyJsons[k].type));
                    enemy.SetEnemyPosition(mapManager.spawnPointJsons[i].waveJsons[j].enemyJsons[k].positionEnemy);
                    enemy.SetInCurrentWave();
                }

                //list line
                for (int l = 0; l < mapManager.spawnPointJsons[i].waveJsons[j].line.Count; l++)
                {
                    GameObject go = Instantiate(EUImanager.Instance.prefab_destination, EUImanager.Instance.world);

                    currentWave.list_destination.Add(go.GetComponent<NewDestination>());
                    go.GetComponent<NewDestination>().SetNumber(currentWave.list_destination.Count - 1);
                    go.GetComponent<NewDestination>().SetPosition(mapManager.spawnPointJsons[i].waveJsons[j].line[l]);
                }


                EUImanager.Instance.SaveLines();

                EUImanager.Instance.CloseEnemyPanel();
                EUImanager.Instance.CloseSpawnPointPanel();


            }
        }


        //barrier
        for (int i = 0; i < mapManager.listBarriers.Count; i++)
        {
            Mapeditor.Instance.GetSpawnPoint(mapManager.listBarriers[i].address).WhatIs(1);
        }

        #endregion

        buttonChoice = 0;
    }

    public void DefaultValue()
    {

    }

    #endregion

}

#region json

[Serializable]
public class MapJson
{
    public double rewardCoin;

    public double bonusCoin;

    // level
    public int numberlevel;
    //end point
    public Vector2 stopPoint;

    public int addressStopPoint;

    //list spawn point
    public List<SpawnPointJson> spawnPointJsons;

    public List<ListBarrier> listBarriers;

    public void SetListSpawnPointJson(List<SpawnPointJson> spawnPointJsons)
    {
        this.spawnPointJsons = spawnPointJsons;
    }

}


[Serializable]
public class SpawnPointJson
{
    public int address;
    // pos spawn point
    public Vector2 posSpawnPoint;
    //list wave
    public List<WaveJson> waveJsons;

    public SpawnPointJson(Vector2 pos, List<WaveJson> waveJsons)
    {
        this.posSpawnPoint = pos;
        this.waveJsons = waveJsons;
    }

}

[Serializable]
public class WaveJson
{
    // number of weapon
    public int numberWeapon;
    // spawn in stt
    public bool spawnInLine;
    // delta time to next
    // == 0 if spawn now
    // != 0 if have time
    public float timeToNext;
    // destroy all enemy to next wave
    public bool destroyAllToNext;
    // list enemy
    public List<EnemyJson> enemyJsons;
    //list destination if enemy folow the line
    public List<Vector2> line;

    public Vector2 followFormation;

    public WaveJson(int numberWeapon,bool spawnInLine,float deltaTime, List<EnemyJson> enemyJsons, bool destroyAllToNext, List<Vector2> destinations,Vector2 formation)
    {
        this.numberWeapon = numberWeapon;
        this.spawnInLine = spawnInLine;
        this.timeToNext = deltaTime;
        this.enemyJsons = enemyJsons;
        this.destroyAllToNext = destroyAllToNext;
        this.line = destinations;
        followFormation = formation;
    }

}

[Serializable]
public class EnemyJson
{
    // position spawn
    public Vector2 positionEnemy;
    // id enemy
    public int id;
    //type
    public int type;

    public EnemyJson(Vector2 position, int id, int numType)
    {
        this.positionEnemy = position;
        this.id = id;
        this.type = numType;
    }

}

[Serializable]
public class ListBarrier
{
    public int address;

    public Vector2 position;
    public int id;
    public int type;
}


#endregion
