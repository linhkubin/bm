﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteManager : MonoBehaviour
{

    private static SpriteManager instance;
    public static SpriteManager Instance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<SpriteManager>();
            }
            return instance;
        }
    }
    [Header("Bonus")]
    public EnemySprite[] list_bonus;

    [Header("Booster")]
    public EnemySprite[] list_booster;

    [Header("Gold")]
    public EnemySprite gold;

    [Header("Enemy Target")]
    public EnemySprite[] list_enemyTarget;

    [Header("Enemy Move")]
    public EnemySprite[] list_enemyMove;

    [Header("List Boss")]
    public EnemySprite[] list_boss;

    public Sprite GetSpriteEnemy(int id, int type)
    {
        if (type == Constant.TYPE_ENEMY_TARGET)
        {
            return list_enemyTarget[id].sprite;
        }
        else
        if (type == Constant.TYPE_ENEMY_MOVE)
        {
            return list_enemyMove[id].sprite;
        }
        else
        {
            Debug.Log("null sprite : " + id + " - " + type);
            return null;

        }
    }
}
