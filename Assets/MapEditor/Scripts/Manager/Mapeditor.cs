﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mapeditor : MonoBehaviour
{
    private static Mapeditor instance;
    public static Mapeditor Instance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<Mapeditor>();
            }
            return instance;
        }
    }

    public List<ESpawnPoints> list_allSpawnPoints = new List<ESpawnPoints>();

    public ESpawnPoints GetSpawnPoint(int addres)
    {
        int row = addres / 100;
        int col = addres % 100;

        return list_allSpawnPoints[row * 13 + col];

    }

}
