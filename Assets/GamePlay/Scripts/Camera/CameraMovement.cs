﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour
{

    private static CameraMovement instance;
    public static CameraMovement Instance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<CameraMovement>();
            }
            return instance;
        }
    }

    private Transform tf_world;
    public Transform GetTranform()
    {
        if (tf_world == null)
        {
            tf_world = transform;
        }
        return tf_world;
    }

    public bool cameraMoving = false;

    // Update is called once per frame
    void Update()
    {
        if (cameraMoving && LevelManager.Instance.startLevel && !Player.Instance.isDead)
        {
            GetTranform().Translate(Vector3.down * Time.deltaTime * 0.5f, Space.World);

            //update process line
            LevelManager.Instance.processLine.Update();
        }
    }

    public void WorldReset()
    {
        GetTranform().position = Vector3.zero;
    }
}
