﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    private static GameManager instance;
    public static GameManager Instance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<GameManager>();
            }
            return instance;
        }
    }

    public int current_Level;
    public int current_BaseWeapon;
    public int current_MC;

    // Start is called before the first frame update
    void Start()
    {

        //Debug.Log(Screen.width +"__"+ Screen.height); 
        //GetComponent<HorizontalLayoutGroup>().enabled = false;
        current_BaseWeapon = 1;
        current_Level = 8;

        Input.multiTouchEnabled = false;

        //Debug.Log("----" + Data.Coin_Reward(1));
        //Debug.Log("----" + Data.Coin_Reward(2));
        //Debug.Log("----" + Data.Coin_Reward(3));
        //Debug.Log("----" + Data.Coin_Reward(7));
        //Debug.Log("----" + Data.Coin_Reward(19));
        //Debug.Log("----" + Data.Coin_Reward(23));
        //Debug.Log("----" + Data.Coin_Reward(57));
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    
}
