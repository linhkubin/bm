﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoolManager : MonoBehaviour
{

    private static PoolManager instance;
    public static PoolManager Instance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<PoolManager>();
            }
            return instance;
        }
    }

    public Transform tf_world;

    [Header("enemy")]

    public GameObject spawnPointPrefab;

    public GameObject wavePrefab;

    public GameObject enemyDeathPrefab;

    [Header("obstacle")]

    public GameObject barrierPrefab;

    [Header("Power")]
    public GameObject powerBonusPrefab;

    public GameObject powerBoosterPrefab;

    [Header("Weapon")]
    public GameObject weaponDropPrefab;

    public GameObject slashPrefab;

    private void Start()
    {
        #region spawn point
        SimplePool.Preload(spawnPointPrefab, 5, "SpawnPoint");
        List<GameObject> go = new List<GameObject>();
        for (int i = 0; i < 5; i++)
        {
            go.Add(SimplePool.Spawn("SpawnPoint", Vector3.zero, Quaternion.identity));

            go[i].transform.SetParent(tf_world, false);
        }

        for (int i = 0; i < 5; i++)
        {
            SimplePool.Despawn(go[i]);
        }
        #endregion

        #region wave
        SimplePool.Preload(wavePrefab, 5, "WaveEnemy");
        #endregion


        #region enemy
            SimplePool.Preload(EnemyManager.Instance.enemyGold_prefab, 5, "enemyGold");
            SimplePool.Preload(EnemyManager.Instance.enemyBonus_prefabs, 2, "enemyBonus");
            SimplePool.Preload(EnemyManager.Instance.enemyBooster_prefabs, 2, "enemyBooster");
            SimplePool.Preload(EnemyManager.Instance.enemyTarget_prefabs, 5, "enemyTarget");
            SimplePool.Preload(EnemyManager.Instance.enemyMove_prefabs, 5, "enemyMove");
            SimplePool.Preload(EnemyManager.Instance.enemyInLine_prefabs, 5, "enemyInLine");

            SimplePool.Preload(enemyDeathPrefab, 5, "EnemyDeath");

        #endregion

        #region Power
            SimplePool.Preload(powerBonusPrefab, 2, "powerBonus");
            SimplePool.Preload(powerBoosterPrefab, 2, "powerBooster");
        #endregion

        #region weapon drop
        SimplePool.Preload(weaponDropPrefab, 9, "WeaponDrop");
        #endregion

        #region slash
        SimplePool.Preload(slashPrefab, 9, "Slash");
        #endregion

    }
}
