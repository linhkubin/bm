﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MCManager : MonoBehaviour
{
    private static MCManager instance;
    public static MCManager Instance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<MCManager>();
            }
            return instance;
        }
    }
    [Header("input MC")]

    public MCPSprite[] list_MC_Sprite;

    public Sprite[] list_Base_Weapon;

    public Sprite[] list_Base_Weapon_lock;

    [Header("input MC")]

    public SpriteRenderer mc;

    public SpriteRenderer mcMainMenu;

    public List<SpriteRenderer> list_BaseWeapon;

    public List<SpriteRenderer> list_MCWeapon;

    private int currentMC;

    private int currentWeapon;

    [Header("UI Weapon")]

    public Image UI_shop_BaseWeapon;

    public Image UI_Upgrade_BaseWeapon;

    public Image UI_shop_MCWeapon;

    public Image UI_Upgrade_MCWeapon;

    private void Start()
    {
        ChangeWeaponMC(0,1);
    }

    public Sprite GetSpriteWeapon()
    {
        return list_Base_Weapon[currentWeapon];
    }

    public void ChangeWeaponMC(int baseWeapon = -1,int numberMC = -1)
    {
        // sprite mc
        if (numberMC != -1)
        {
            //MC in main
            currentMC = numberMC;
            mc.sprite = list_MC_Sprite[numberMC].MC;

            mcMainMenu.sprite = list_MC_Sprite[numberMC].MC;
        }

        //sprite base weapon

        if (baseWeapon != -1)
        {

            currentWeapon = baseWeapon;

            for (int i = 0; i < list_BaseWeapon.Count; i++)
            {
                list_BaseWeapon[i].sprite = list_Base_Weapon[baseWeapon];
            }

            // shop
            UI_shop_BaseWeapon.sprite = list_Base_Weapon[baseWeapon];
            UI_Upgrade_BaseWeapon.sprite = list_Base_Weapon[baseWeapon];

        }

        //sprite MC weapon
        if (numberMC != -1)
        {
            // mc in main + MC in game
            for (int i = 0; i < list_MCWeapon.Count; i++)
            {
                list_MCWeapon[i].sprite = list_MC_Sprite[numberMC].MCWeapon;
            }

            //shop
            UI_shop_MCWeapon.sprite = list_MC_Sprite[numberMC].MCWeapon;
            UI_Upgrade_MCWeapon.sprite = list_MC_Sprite[numberMC].MCWeapon;
        }
        
    }

}

[Serializable]
public class MCPSprite
{
    [Header("MC")]
    public string name;

    public Sprite MC;

    public Sprite MCWeapon;

    public int currentLevel = 1;

    public GameObject mc_MainMenu;

    public GameObject mc_InGame;

    // level

    public void SetLevelWeapon(int level)
    {
        currentLevel = level;
    }

    public void UpLevelWeapon()
    {
        currentLevel++;
    }
    // data
    #region DamageUpgrade()

    public double Sub1Base = 12;
    public double Sub1GrowthStep = 10;
    public double Sub1ScaleBase = 0.025d;
    public double Sub1ScaleStep = 0.022d;

    public double DamageUpgrade()
    {
        double Sub1Growth = Math.Floor(currentLevel / Sub1GrowthStep);

        double Sub1Scale = Math.Round(Sub1ScaleBase * currentLevel * (1 + Sub1Growth * Sub1ScaleStep), 5, MidpointRounding.AwayFromZero);

        double Sub1 = Math.Floor(Sub1Base * (1 + Sub1Growth + Math.Round(currentLevel * Sub1Scale, 5, MidpointRounding.AwayFromZero)));

        return Sub1;
    }
    #endregion

    #region CoinUpgrade()

    readonly double CostBase = 56;

    readonly double CostGrowthStep = 10;

    readonly double CostScaleBase = 0.08d;

    readonly double CostScaleStep = 0.8d;

    public double CoinUpgrade()
    {
        double CostGrowth = (int)Math.Floor(currentLevel / CostGrowthStep);

        double CostScale = Math.Round(CostScaleBase * currentLevel * (1 + CostGrowth * CostScaleStep), 5, MidpointRounding.AwayFromZero);

        double Cost = Math.Floor(CostBase * (1 + CostGrowth + Math.Round(currentLevel * CostScale, 5, MidpointRounding.AwayFromZero)));

        return Cost;
    }

    #endregion

    //public Animator animWeapon;
}
