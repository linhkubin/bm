﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyManager : MonoBehaviour
{

    private static EnemyManager instance;
    public static EnemyManager Instance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<EnemyManager>();
            }
            return instance;
        }
    }

    public GameObject enemyGold_prefab;

    public GameObject enemyBonus_prefabs;

    public GameObject enemyBooster_prefabs;

    public GameObject enemyTarget_prefabs;

    public GameObject enemyMove_prefabs;

    public GameObject enemyInLine_prefabs;

    public GameObject[] enemyBoss_prefabs;

}
