﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    private static UIManager instance;
    public static UIManager Instance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<UIManager>();
            }
            return instance;
        }
    }

    [Header("UI")]
    public GameObject UI_Pause;

    public GameObject UI_Pre;

    public GameObject UI_Revive;

    private Coroutine coroutineStop;

    #region pause
    public void ActiveUIPause()
    {
        //Time.timeScale = 0.05f;
        if (LevelManager.Instance.startLevel)
        {
            coroutineStop = StartCoroutine(ActiveStop());
        }
    }

    public void InactiveUIPause()
    {
        activeStop = false;

        if (coroutineStop != null)
        {
            StopCoroutine(coroutineStop);
        }

        Time.timeScale = 1;

        UI_Pause.SetActive(false);
    }


    bool activeStop = false;
    private IEnumerator ActiveStop()
    {
        yield return new WaitForSeconds(0.5f);
        Time.timeScale = 0.05f;
        UI_Pause.SetActive(true);
    }
    #endregion

    #region UI pre play
    public void OpenUIPre()
    {
        if (Random.Range(0,2) == 0)
        {
            UI_Pre.SetActive(true);
        }
        else
        {
            LevelManager.Instance.StartLevel();
        }
    }

    public void CloseUIPre()
    {
        UI_Pre.SetActive(false);
        LevelManager.Instance.StartLevel();

    }

    public void CloseUIPreWithColleague()
    {
        UI_Pre.SetActive(false);
        LevelManager.Instance.StartLevel();

        //
    }
    #endregion

    #region UI revive
    public void OpenUIRevive()
    {
        InactiveUIPause();

        StartCoroutine(RunUIRevive());
    }

    private IEnumerator RunUIRevive()
    {
        yield return new WaitForSeconds(2f);
        UI_Revive.SetActive(true);
        yield return new WaitForSeconds(2f);
        UI_Revive.SetActive(false);

        Time.timeScale = 0.00f;
        UIMainScene.Instance.ActiveDefeat();
    }
    #endregion

}
