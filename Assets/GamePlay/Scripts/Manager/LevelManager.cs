﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour
{

    private static LevelManager instance;
    public static LevelManager Instance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<LevelManager>();
            }
            return instance;
        }
    }

    [Header("weapon")]
    public List<GameObject> listWeapon = new List<GameObject>();

    [Header("area attack")]

    public Transform areaTop;

    public Transform areaBot;

    [Header("current")]

    public bool startLevel = false;

    private int numberSpawnPoint = 0;

    //public int currentLevel = 0;

    //public int i_numberEnemy;
    [Header("somthing")]

    public Transform tf_world;

    public EndMoveCameraPoint stopPoint;

    public List<SpawnPoint> list_SpawnPoint;

    public Transform startPoint;

    [Header("process line")]

    public Transform start_ProcessLine;

    public Transform end_ProcessLine;

    public ProcessLine processLine;
    
    void Start()
    {
        processLine.SetTransform(start_ProcessLine, end_ProcessLine);
        //startLevel = true;
        InitStage();
    }

    public void ReadJson()
    {

        MapJson mapManager = new MapJson();

        TextAsset assets = Resources.Load(GameManager.Instance.current_Level.ToString()) as TextAsset;

        //Debug.Log(assets);
        //Debug.Log(GameManager.Instance.current_Level);

        mapManager = JsonUtility.FromJson<MapJson>(assets.text);

        stopPoint.SetPosition(mapManager.stopPoint);

        // spawn point

        for (int i = 0; i < mapManager.spawnPointJsons.Count; i++)
        {
            SpawnPoint spawn = SimplePool.Spawn("SpawnPoint", mapManager.spawnPointJsons[i].posSpawnPoint, Quaternion.identity).GetComponent<SpawnPoint>();

            // init wave
            for (int j = 0; j < mapManager.spawnPointJsons[i].waveJsons.Count; j++)
            {
                WaveEnemy wave = SimplePool.Spawn("WaveEnemy", Vector3.zero, Quaternion.identity).GetComponent<WaveEnemy>();

                //init enemy in wave
                for (int k = 0; k < mapManager.spawnPointJsons[i].waveJsons[j].enemyJsons.Count; k++)
                {

                    // check enemy
                    int typeNewEnemy = mapManager.spawnPointJsons[i].waveJsons[j].enemyJsons[k].type;
                    int idNewEnemy = mapManager.spawnPointJsons[i].waveJsons[j].enemyJsons[k].id;
                    if (typeNewEnemy == Constant.TYPE_ENEMY_BONUS)
                    {
                        Enemy enemy = SimplePool.Spawn("enemyBonus" , mapManager.spawnPointJsons[i].waveJsons[j].enemyJsons[k].positionEnemy, Quaternion.identity).GetComponent<Enemy>();
                        wave.SetEnemy(enemy, idNewEnemy, typeNewEnemy);
                    }
                    else if (typeNewEnemy == Constant.TYPE_ENEMY_BOOSTER)
                    {
                        Enemy enemy = SimplePool.Spawn("enemyBooster" , mapManager.spawnPointJsons[i].waveJsons[j].enemyJsons[k].positionEnemy, Quaternion.identity).GetComponent<Enemy>();
                        wave.SetEnemy(enemy, idNewEnemy, typeNewEnemy);
                    }
                    else if (typeNewEnemy == Constant.TYPE_ENEMY_GOLD)
                    {
                        Enemy enemy = SimplePool.Spawn("enemyGold" , mapManager.spawnPointJsons[i].waveJsons[j].enemyJsons[k].positionEnemy, Quaternion.identity).GetComponent<Enemy>();
                        wave.SetEnemy(enemy, idNewEnemy, typeNewEnemy);
                    }
                    else if (mapManager.spawnPointJsons[i].waveJsons[j].line.Count > 0)
                    {
                        Enemy enemy = SimplePool.Spawn("enemyInLine" , mapManager.spawnPointJsons[i].waveJsons[j].enemyJsons[k].positionEnemy, Quaternion.identity).GetComponent<Enemy>();
                        enemy.GetComponent<Inline>().SetListLine(mapManager.spawnPointJsons[i].waveJsons[j].line);
                        wave.SetEnemy(enemy, idNewEnemy, typeNewEnemy);
                    }
                    else if (typeNewEnemy == Constant.TYPE_ENEMY_TARGET)
                    {
                        Enemy enemy = SimplePool.Spawn("enemyTarget", mapManager.spawnPointJsons[i].waveJsons[j].enemyJsons[k].positionEnemy, Quaternion.identity).GetComponent<Enemy>();
                        wave.SetEnemy(enemy, idNewEnemy, typeNewEnemy);
                    }
                    else if (typeNewEnemy == Constant.TYPE_ENEMY_MOVE)
                    {
                        Enemy enemy = SimplePool.Spawn("enemyMove", mapManager.spawnPointJsons[i].waveJsons[j].enemyJsons[k].positionEnemy, Quaternion.identity).GetComponent<Enemy>();
                        wave.SetEnemy(enemy, idNewEnemy, typeNewEnemy);
                    }
                    else if (typeNewEnemy == Constant.TYPE_ENEMY_BOSS)
                    {
                        Enemy enemy = SimplePool.Spawn("enemyBoss", mapManager.spawnPointJsons[i].waveJsons[j].enemyJsons[k].positionEnemy, Quaternion.identity).GetComponent<Enemy>();
                        wave.SetEnemy(enemy, idNewEnemy, typeNewEnemy);
                    }

                }
                spawn.AddWave(wave);

                //number weapon + in line + defeat All + time next
                wave.SetInit(mapManager.spawnPointJsons[i].waveJsons[j].numberWeapon, mapManager.spawnPointJsons[i].waveJsons[j].spawnInLine, mapManager.spawnPointJsons[i].waveJsons[j].destroyAllToNext, mapManager.spawnPointJsons[i].waveJsons[j].timeToNext);
            }

            list_SpawnPoint.Add(spawn);
        }


    }

    public void ResetStage()
    {
        startLevel = false;
        for (int i = 0; i < list_SpawnPoint.Count; i++)
        {
            for (int j = 0; j < list_SpawnPoint[i].listWave.Count; j++)
            {
                for (int k = 0; k < list_SpawnPoint[i].listWave[j].enemies.Count; k++)
                {
                    SimplePool.Despawn(list_SpawnPoint[i].listWave[j].enemies[k].gameObject);
                }

                SimplePool.Despawn(list_SpawnPoint[i].listWave[j].gameObject);
            }

            list_SpawnPoint[i].Reset();
            SimplePool.Despawn(list_SpawnPoint[i].gameObject);
        }
        list_SpawnPoint.Clear();

        CameraMovement.Instance.cameraMoving = false;

        //reset weapon
        for (int i = 0; i < listWeapon.Count; i++)
        {
            if (listWeapon[i].activeInHierarchy)
            {
                SimplePool.Despawn(listWeapon[i]);
            }
        }
        listWeapon.Clear();
    }

    public void InitStage()
    {
        ReadJson();
        Player.Instance.GetTransform().position = startPoint.position;
        numberSpawnPoint = list_SpawnPoint.Count;

        //
        processLine.Initialize();
    }

    public void StartLevel()
    {
        startLevel = true;
        CameraMovement.Instance.cameraMoving = true;

        //set anim cho player
        Player.Instance.PlayerState("start");
    }

    public void FinishLevel(bool win)
    {
        if (win)
        {
            Player.Instance.isDead = true;
            UIManager.Instance.InactiveUIPause();
            LevelManager.Instance.startLevel = false;
            //StartCoroutine(UIMainScene.Instance.LevelWin());
            Player.Instance.PlayerState("win");
            Player.Instance.MoveNextLevel();

        }
        else
        {
            UIManager.Instance.OpenUIRevive();
        }
    }

    public void FinisSpawnPoint()
    {
        numberSpawnPoint--;
        if (numberSpawnPoint <= 0)
        {
            FinishLevel(true);
        }
    }
}
