﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpgradeManager : MonoBehaviour
{
    private static UpgradeManager instance;
    public static UpgradeManager Instance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<UpgradeManager>();
            }
            return instance;
        }
    }

    enum Shop { UpgradeBase, UpgradeMain, Shop }

    public GameObject G_shop;

    public GameObject g_buttonCloseShop;

    public GameObject[] g_shop;

    public Animator[] anim_shop;

    public Animator[] anim_buttonShop;

    int currentShop = -1;

    public void OpenShop(int shop)
    {
        if (currentShop == shop)
        {
            CloseShop();
            return;
        }

        if (currentShop != -1)
        {
            Deactive(currentShop);

        }

        Active(shop);
        currentShop = shop;

    }

    public void CloseShop()
    {
        if (currentShop != -1)
        {
            Deactive(currentShop, true);

            currentShop = -1;
        }
        
    }

    private void Active(int shop)
    {
        if (g_shop[shop] != null)
        {
            // chua co ui shop
            G_shop.SetActive(true);
            g_shop[shop].SetActive(true);
            g_buttonCloseShop.SetActive(true);

            // xu ly MC
            if (shop == 0)
            {
                if (currentShop == 1)
                {
                    UIMainScene.Instance.MC_ShowDefault();
                }
                UIMainScene.Instance.MC_ShowBase_Weapon();
            }
            if (shop == 1)
            {
                if (currentShop == 0)
                {
                    UIMainScene.Instance.MC_ShowDefault();
                }
                UIMainScene.Instance.MC_ShowMC_Weapon();
            }
            if (shop == 2)
            {
                UIMainScene.Instance.MC_ShowDefault();
            }
        }

        anim_buttonShop[shop].SetTrigger("open");
    }

    private void Deactive(int shop,bool offShop = false)
    {
        if (g_shop[shop] != null)
        {
            // chua co ui shop
            anim_shop[shop].SetTrigger("close");
        }
        anim_buttonShop[shop].SetTrigger("close");
        StartCoroutine(DeactiveShop(shop, offShop));
    }

    private IEnumerator DeactiveShop(int shop,bool offShop = false)
    {
        yield return new WaitForSeconds(0.25f);
        if (g_shop[shop] != null)
        {
            g_shop[shop].SetActive(false);
        }

        if (offShop)
        {
            g_buttonCloseShop.SetActive(false);
            G_shop.SetActive(false);
            UIMainScene.Instance.MC_ShowDefault();
        }
    }
}
