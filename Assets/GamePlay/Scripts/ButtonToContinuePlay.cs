﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonToContinuePlay : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        GetComponent<Button>().onClick.AddListener(InactivePanel);
    }

    void InactivePanel()
    {
        UIManager.Instance.InactiveUIPause();
    }
}
