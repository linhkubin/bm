﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnPoint : MonoBehaviour
{
    public List<WaveEnemy> listWave;
    int finishWave = 0;
    int currentWave = 0; 

    //private void Start()
    //{
    //    for (int i = 0; i < listWave.Count; i++)
    //    {
    //        listWave[i].Initialize(GetComponent<SpawnPoint>());
    //    }
    //}

    public void ActiveEnemy()
    {
        listWave[currentWave].ActiveEnemy();
    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Spawn")
        {
            Debug.Log("active enemy in 1 wave");
            ActiveEnemy();
        }
    }

    public void NextWay()
    {
        currentWave++;
        if (listWave.Count > currentWave)
        {
            ActiveEnemy();
        }else
        {
            //LevelManager.Instance.FinisSpawnPoint();
        }
    }

    public void FinisWave()
    {
        finishWave++;
        if (finishWave >= listWave.Count)
        {
            LevelManager.Instance.FinisSpawnPoint();
        }
    }

    public void SetPosition(Vector2 pos)
    {
        transform.position = pos;
    }

    public void AddWave(WaveEnemy wave)
    {
        listWave.Add(wave);
        wave.Initialize(GetComponent<SpawnPoint>());

    }

    public void Reset()
    {
        finishWave = 0;
        currentWave = 0;
        for (int i = 0; i < listWave.Count; i++)
        {
            listWave[i].Reset();
        }
        listWave.Clear();
    }
}


