﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndMoveCameraPoint : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Spawn")
        {
            //Debug.Log("stop");
            CameraMovement.Instance.cameraMoving = false;
        }
    }

    public void SetPosition(Vector2 position)
    {
        transform.position = new Vector3(0, position.y);
    }

}
