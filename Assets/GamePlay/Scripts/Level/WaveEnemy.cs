﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaveEnemy : MonoBehaviour
{
    public List<Enemy> enemies = new List<Enemy>();
    //public bool wayEnd = false;
    private SpawnPoint spawnPoint;

    private int numberEnemy;

    private bool spawnInLine = false;

    //private int numberWeapon;

    float timeToNext = 0;

    bool defeatAllToNext = false;

    bool spawnByTime = false;

    public void Initialize(SpawnPoint point)
    {
        spawnPoint = point;
        //wayEnd = false;
        numberEnemy = enemies.Count;

        //for (int i = 0; i < numberEnemy; i++)
        //{
        //    enemies[i].SetWave(GetComponent<WaveEnemy>());
        //}
    }

    public void EnemyDeath()
    {
        numberEnemy--;
        if (numberEnemy <= 0)
        {
            //wayEnd = true;
            if (defeatAllToNext)
            {
                spawnPoint.NextWay();
            }
            else if (!spawnByTime)
            {
                spawnPoint.NextWay();
            }
            spawnPoint.FinisWave();
        }
    }

    //public void SetParentPoint(SpawnPoint point)
    //{
    //    spawnPoint = point;
    //}

    public void ActiveEnemy()
    {
        //Debug.Log("spawnInLine" + spawnInLine);
        if (spawnInLine)
        {
            StartCoroutine(ActiveEnemies(1f));
        }
        else
        {
            StartCoroutine(ActiveEnemies(0f));
        }


        //set time to next
        if (!defeatAllToNext)
        {
            StartCoroutine(NextWave(timeToNext));
        }
    }

    private IEnumerator NextWave(float timer)
    {
        yield return new WaitForSeconds(timer);
        if (numberEnemy > 0)
        {
            spawnByTime = true;

            spawnPoint.NextWay();
        }
    }

    private IEnumerator ActiveEnemies(float delay)
    {
        for (int i = 0; i < enemies.Count; i++)
        {
            enemies[i].Active();
            yield return new WaitForSeconds(delay);
        }
    }

    public void SetEnemy(Enemy enemy, int id, int type)
    {
        enemies.Add(enemy);
        enemy.SetWave(GetComponent<WaveEnemy>(), id, type);
    }

    public void SetInit(int numberWeap, bool line = false,bool defeatAll = false,float timeNext = 0)
    {

        spawnInLine = line;

        if (numberWeap !=0)
        {
            SetWeaponForEnemy(numberWeap);
        }

        defeatAllToNext = defeatAll;
        timeToNext = timeNext;
    }

    private void SetWeaponForEnemy(int numberWeapon)
    {
        while (numberWeapon > 0)
        {
            int ran = UnityEngine.Random.Range(0, enemies.Count);
            if (enemies[ran].haveKnife)
            {
                continue;
            }
            else
            {
                enemies[ran].haveKnife = true;
                numberWeapon--;
            }
        }
    }

    public void Reset()
    {
        enemies.Clear();

        numberEnemy = 0;

        spawnInLine = false;

        spawnByTime = false;

        //numberWeapon = 0;
    }

}
