﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Data : MonoBehaviour
{
    #region Reward

    static double RewardBase = 100;
    static double RewardGrowthStep = 15;

    static double RewardScaleBase = 0.1d;

    static double RewardScaleStep = 0.12d;

    static double BonusRate = 0.5d;

    // coin from all enemy 
    public static double Coin_Reward(int level)
    {
        double RewardGrowth = (int)Math.Floor(level / RewardGrowthStep);

        double RewardScale = Math.Round(RewardScaleBase * level * (1 + RewardGrowth * RewardScaleStep), 5, MidpointRounding.AwayFromZero);

        double Reward = (int)Math.Floor(RewardBase * (1 + RewardGrowth + Math.Round(level * RewardScale, 5, MidpointRounding.AwayFromZero)));

        double Bonus = Reward * (1 + BonusRate);

        return Reward;
    }

    //coin for each weapon claim
    public static double Coin_Bonus(int level)
    {
        double RewardGrowth = (int)Math.Floor(level / RewardGrowthStep);

        double RewardScale = Math.Round(RewardScaleBase * level * (1 + RewardGrowth * RewardScaleStep), 5, MidpointRounding.AwayFromZero);

        double Reward = (int)Math.Floor(RewardBase * (1 + RewardGrowth + Math.Round(level * RewardScale, 5, MidpointRounding.AwayFromZero)));

        double Bonus = Reward * (BonusRate);

        return Math.Floor(Bonus / 9);
    }

    #endregion

    #region Cost

    static double CostBase = 56;

    static double CostGrowthStep = 10;

    static double CostScaleBase = 0.08d;

    static double CostScaleStep = 0.8d;


    public static double Cost_Upgrade_Base(int level)
    {
        double CostGrowth = (int)Math.Floor(level / CostGrowthStep);

        double CostScale = Math.Round(CostScaleBase * level * (1 + CostGrowth * CostScaleStep), 5, MidpointRounding.AwayFromZero);

        double Cost = (int)Math.Floor(CostBase * (1 + CostGrowth + Math.Round(level * CostScale, 5, MidpointRounding.AwayFromZero)));

        return Cost;
    }

    #endregion

    #region Base Damage 

    static double MainBase = 1;

    static double MainGrowthStep = 10;

    static double MainScaleBase = 0.25d;

    static double MainScaleStep = 0.025d;


    public static double Damage_Base(int level)
    {
        double MainGrowth = (int)Math.Floor(level / MainGrowthStep);

        double MainScale = Math.Round(MainScaleBase * level * (1 + MainGrowth * MainScaleStep), 5, MidpointRounding.AwayFromZero);

        double Main = (int)Math.Floor(MainBase * (1 + MainGrowth + Math.Round(level * MainScale, 5, MidpointRounding.AwayFromZero)));

        return Main;
    }

    #endregion

    public string _PrettyInt_ToString(double number)
    {
        return null;
    }

}

