﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class ProcessLine 
{
    [SerializeField]
    private Bar bar;

    private Transform maxPoint;

    private Transform currentPoint;

    private float maxVal;

    private float currentVal;

    public float MaxVal
    {
        get
        {
            return maxVal;
        }

        set
        {
            this.maxVal = value;
            bar.MaxValue = maxVal;
        }
    }

    public float CurrentVal
    {
        get
        {
            return currentVal;
        }

        set
        {
            this.currentVal = Mathf.Clamp(value, 0, maxVal);
            bar.Value = currentVal;
        }
    }

    public void SetTransform(Transform current,Transform max)
    {
        currentPoint = current;
        maxPoint = max;

    }

    public void Initialize()
    {
        MaxVal = maxPoint.position.y - currentPoint.position.y;
        CurrentVal = 0;
    }

    public void Update()
    {
        CurrentVal = MaxVal - maxPoint.position.y + currentPoint.position.y;
    }
}
