﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Despawn : MonoBehaviour
{
    public float timmer;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(DespawnGameOject());
    }

    private IEnumerator DespawnGameOject()
    {
        yield return new WaitForSeconds(timmer);
        SimplePool.Despawn(gameObject);
    }
}
