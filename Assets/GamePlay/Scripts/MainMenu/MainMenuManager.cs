﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuManager : MonoBehaviour
{

    private static MainMenuManager instance;
    public static MainMenuManager Instance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<MainMenuManager>();
            }
            return instance;
        }
    }

   
}
