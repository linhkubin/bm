﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMainMenu : MonoBehaviour
{
    public void StartLevel()
    {
        Player.Instance.PlayerActive(true);
        UIManager.Instance.OpenUIPre();
        UIMainScene.Instance.ActiveMainScene(false);
    }
}
