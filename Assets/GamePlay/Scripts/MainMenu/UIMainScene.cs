﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIMainScene : MonoBehaviour
{

    private static UIMainScene instance;
    public static UIMainScene Instance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<UIMainScene>();
            }
            return instance;
        }
    }

    public GameObject tapToPlay;

    [Header("main menu")]
    public Animator anim_UI_MainMenu;

    public Animator anim_MC_MainMenu;

    public GameObject g_mainScene;

    public GameObject g_mainMenu;

    [Header("defeat")]
    public GameObject g_defeat;

    public Animator anim_defeat;

    [Header("victory")]

    public GameObject g_victory;

    public Animator anim_victory;



    #region MC_anim

    public void MC_ShowMC_Weapon()
    {
        tapToPlay.SetActive(false);
        anim_MC_MainMenu.SetTrigger("showMCWeapon");
        
    }

    public void MC_ShowBase_Weapon()
    {
        tapToPlay.SetActive(false);
        anim_MC_MainMenu.SetTrigger("showBaseWeapon");
    }

    public void MC_ShowDefault()
    {
        tapToPlay.SetActive(true);
        anim_MC_MainMenu.SetTrigger("default");
    }

    public void MC_State(string state)
    {

        /*
         play
         showMCWeapon
         showBaseWeapon
         default
         defeattomain
         vitorytomain
         defeat
         vitory
         */
        anim_MC_MainMenu.SetTrigger(state);
    }

    #endregion

    public void PlayButton()
    {
        //Debug.Log("play anim");

        Play();


        //Player.Instance.PlayerActive(true);
        //UIManager.Instance.OpenUIPre();
        //ActiveMainScene(false);
    }

    public void Play()
    {
        anim_UI_MainMenu.SetTrigger("play");
        anim_MC_MainMenu.SetTrigger("play");

    }

    public void ActiveMainScene(bool active)
    {
        g_mainScene.SetActive(active);
    }

    public IEnumerator BackMainScene(GameObject ui,Animator anim)
    {
        Reset();

        anim.SetTrigger("deactive");

        yield return new WaitForSeconds(1f);

        ui.SetActive(false);

        g_mainMenu.SetActive(true);

        ActiveMainScene(true);

    }

    private void Reset()
    {
        Player.Instance.PlayerState("reset");
        Player.Instance.PlayerEnableMove(false);
        LevelManager.Instance.processLine.Initialize();

        Player.Instance.moveType.ResetMove();

        Player.Instance.ResetWeapon();
        //update
        Time.timeScale = 1;

        CameraMovement.Instance.WorldReset();
        LevelManager.Instance.ResetStage();
        LevelManager.Instance.InitStage();
    }

    #region UI fail
    public void ActiveDefeat()
    {

        g_mainMenu.SetActive(false);

        LevelManager.Instance.startLevel = false;
        g_defeat.SetActive(true);
        g_mainScene.SetActive(true);
        MC_State("defeat");

    }

    public void Button_x1_inDefeat()
    {
        StartCoroutine(BackMainScene(g_defeat, anim_defeat));
        MC_State("defeattomain");
    }

    public void Button_x3_inDefeat()
    {
        //MC_State("defeattomain");

    }
    public void Button_x10_inDefeat()
    {
        //MC_State("defeattomain");

    }

    #endregion


    #region UI victory


    public IEnumerator LevelWin()
    {
        yield return new WaitForSeconds(2f);
        ActiveVictory();

    }

    public void ActiveVictory()
    {
        g_mainMenu.SetActive(false);

        g_victory.SetActive(true);
        g_mainScene.SetActive(true);

        MC_State("victory");
    }

    public void Button_x1_inVictory()
    {
        StartCoroutine(BackMainScene(g_victory, anim_victory));
        MC_State("vitorytomain");

    }

    public void Button_x3_inVictory()
    {
        //MC_State("vitorytomain");

    }
    public void Button_x10_inVictory()
    {
        //MC_State("vitorytomain");
    }

    #endregion

    #region upgrade

    public void Shop_baseWeapon()
    {

    }

    public void Shop_mainWeapon()
    {

    }

    public void Shop_Cart()
    {

    }

    public void Button_backMainMenu()
    {

        MC_State("default");
    }

    #endregion
}
