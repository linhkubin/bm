﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveByDrag : MonoBehaviour
{
    private float deltaX, deltaY;
    private Rigidbody2D rigid;
    private Transform tf_player;
    private Transform GetTransform()
    {
        if (tf_player == null)
        {
            tf_player = transform;
        }
        return tf_player;
    }


    private void Start()
    {
        rigid = GetComponent<Rigidbody2D>();
    }

    private void FixedUpdate()
    {
        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);

            Vector2 touchPos = Camera.main.ScreenToWorldPoint(touch.position);

            switch (touch.phase)
            {
                case TouchPhase.Began:
                    deltaX = touchPos.x - GetTransform().position.x;
                    deltaY = touchPos.y - GetTransform().position.y;
                    break;
                case TouchPhase.Moved:
                    rigid.MovePosition(new Vector2(touchPos.x - deltaX, touchPos.y - deltaY));
                    break;
                case TouchPhase.Stationary:
                    break;
                case TouchPhase.Ended:
                    rigid.velocity = Vector2.zero;
                    break;
                case TouchPhase.Canceled:
                    break;
                default:
                    break;
            }
        }
    }

}
