﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spaceship : MonoBehaviour {

	Rigidbody2D rb;
	float dirX;
	float dirY;
	float dir;
	float moveSpeed = 20f;
    Vector2 index;

    // Use this for initialization
    //void Start()
    //{
    //    rb = GetComponent<Rigidbody2D>();
    //    index = new Vector2(Input.acceleration.x, Input.acceleration.y);

    //    Debug.Log("Space ship start");
    //}

    // Update is called once per frame
    //void Update () {
    //       dirX = (Input.acceleration.x - index.x) * moveSpeed;
    //       dirY = (Input.acceleration.y - index.y) * moveSpeed;

    //       transform.position = new Vector2 (Mathf.Clamp (transform.position.x, -7.5f, 7.5f), Mathf.Clamp(transform.position.y, -7.5f, 7.5f));
    //}

    void FixedUpdate()
	{
		rb.velocity = new Vector2 (dirX, dirY);
	}

}
