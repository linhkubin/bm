﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Constant 
{
    public static float diameter = 1f;

    public static int TYPE_ENEMY_TARGET = 1;

    public static int TYPE_ENEMY_MOVE = 2;

    public static int TYPE_ENEMY_INLINE = 3;

    public static int TYPE_ENEMY_GOLD = 4;

    public static int TYPE_ENEMY_BOOSTER = 5;

    public static int TYPE_ENEMY_BONUS = 6;

    public static int TYPE_ENEMY_BOSS = 7;



    #region Color

    public static Color32 color_red = new Color32(0xFF, 0x1A, 0x00, 0xFF);
    public static Color32 color_pink = new Color32(0xEA, 0x00, 0xFF, 0xFF);
    public static Color32 color_blue = new Color32(0x00, 0x60, 0xFF, 0xFF);
    public static Color32 color_green = new Color32(0x00, 0xDA, 0x00, 0xFF);
    public static Color32 color_yellow = new Color32(0xFF, 0xD8, 0x00, 0xFF);

    #endregion
}
