﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inline : Enemy
{

    public  List<Vector2> listPoint;

    Vector3 target;

    public Transform tf_enemy;

    int iline = 0;

    bool inList = false;

    public SpriteRenderer avata;

    public override void Start()
    {
        base.Start();
        inList = false;
        iline = 0;

        avata.sprite = SpriteManager.Instance.GetSpriteEnemy(id, type);
    }


    private void Update()
    {

        if (!isDead && LevelManager.Instance.startLevel && !Player.Instance.isDead && move)
        {

            if (inList)
            {
                Vector3 direction = target - tf_enemy.position;
                Quaternion to = Quaternion.FromToRotation(Vector3.up, direction);

                tf_enemy.Translate(Time.deltaTime * tf_enemy.up * movementSpeed, Space.World);
                tf_enemy.rotation = Quaternion.Lerp(tf_enemy.rotation, to, Time.time);

                inList = direction.sqrMagnitude > 0.02f;
            }
            else
            {
                target = listPoint[iline];
                iline++;
                if (iline == listPoint.Count) iline = 0;
                inList = true;
            }

            
        }
        else
        {
            GetComponent<Rigidbody2D>().velocity = Vector2.zero;
            //chong xoay
        }
    }

    public void SetListLine(List<Vector2> listLine)
    {
        listPoint.Clear();
        listPoint = listLine;
    }

    public override void ChangeDirection()
    {
        base.ChangeDirection();
    }

    public override void Death()
    {
        base.Death();
    }

    public override void Active()
    {
        base.Active();
    }


    public override void OnTriggerEnter2D(Collider2D other)
    {
        //base.OnTriggerEnter2D(other);
        if (!inAttackArea && other.tag == "AttackArea")
        {
            inAttackArea = true;
        }

        if (other.tag == "BaseHitBox")
        {
            TakeDamgeBase();
            // tao slash
            if (!isDead)
            {
                StartCoroutine(Slash(other.transform));
            }
        }

        if (other.tag == "MainHitBox")
        {
            TakeDamgeMain();
        }

        if (other.tag == "DefeatArea")
        {
            Death();
        }
    }

    public override void OnTriggerExit2D(Collider2D collision)
    {
        base.OnTriggerExit2D(collision);
    }


    public override void TakeDamgeBase()
    {
        base.TakeDamgeBase();
    }

    public override void TakeDamgeMain()
    {
        base.TakeDamgeMain();
    }

    public override void TakeEffect()
    {
        base.TakeEffect();
    }


    private IEnumerator Slash(Transform pos)
    {
        yield return new WaitForSeconds(0.05f);

        SimplePool.Spawn("Slash", pos.position, Quaternion.identity);

    }
}
