﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerMove : MonoBehaviour
{
    float angle;

    Quaternion to;

    float movementSpeed = 3;

    public Rigidbody2D MyRigidbody2D;

    public Transform GetTransform;

    // Start is called before the first frame update
    public void Start()
    {
        Vector3 target = new Vector3(Random.Range(LevelManager.Instance.areaBot.position.x, LevelManager.Instance.areaTop.position.x), Random.Range(LevelManager.Instance.areaBot.position.y, LevelManager.Instance.areaTop.position.y), 0);

        Vector3 direction = target - GetTransform.position;

        Quaternion to = Quaternion.FromToRotation(Vector3.up, direction);

        transform.rotation = to;

        angle = transform.eulerAngles.z;

        StartCoroutine(Destroy());
    }

    // Update is called once per frame
    void Update()
    {
        if (LevelManager.Instance.startLevel && !Player.Instance.isDead)
        {
            GetTransform.Translate(Time.deltaTime * transform.up * movementSpeed, Space.World);
            transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.Euler(new Vector3(0, 0, angle)), Time.time * 15f);
        }
        else
        {
            MyRigidbody2D.velocity = Vector2.zero;
            transform.rotation = Quaternion.Euler(new Vector3(0, 0, angle));
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Ngang")
        {
            angle += 180;
            float ran = Random.Range(-20, 20);
            angle = -angle + ran;
        }
        if (collision.tag == "Doc")
        {
            float ran = Random.Range(-20, 20);
            angle = -angle + ran;
        }
    }

    private IEnumerator Destroy()
    {
        for (int i = 0; i < 10; i++)
        {
            yield return new WaitForSeconds(1f);
            if (!LevelManager.Instance.startLevel || Player.Instance.isDead)
            {
                break;
            }
        }

        SimplePool.Despawn(GetTransform.gameObject);
    }
}
