﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;

[Serializable]
public class Stat
{

    [SerializeField]
    private Text valueText;

    [SerializeField]
    private float maxVal;

    [SerializeField]
    private float currentVal;

    [SerializeField]
    private SpriteRenderer colorRender;

    public float MaxVal
    {
        get
        {
            return maxVal;
        }

        set
        {
            this.maxVal = value;
        }
    }

    public float CurrentVal
    {
        get
        {
            return currentVal;
        }

        set
        {
            currentVal = Mathf.Clamp(value, 0, maxVal);
            valueText.text = ConvertValue(currentVal);
            colorRender.color = ReturnColor(currentVal);
            //colorRender.color = Color.Lerp(Color.white, Color.yellow, currentVal / MaxVal);
        }
    }

    public void Initialize()
    {
        this.MaxVal = maxVal;
        this.CurrentVal = maxVal;
    }

    private Color ReturnColor(float currentValue)
    {
        if (currentValue > 500)
        {
            return Constant.color_red;
        }
        else if (currentValue > 400)
        {
            return Color.Lerp(Constant.color_pink, Constant.color_red, (currentVal - 400)/ 100); //delta
        }
        else if(currentValue > 300)
        {
            return Color.Lerp(Constant.color_blue, Constant.color_pink, (currentVal - 300)/ 100);
        }
        else if (currentValue > 200)
        {
            return Color.Lerp(Constant.color_green, Constant.color_blue, (currentVal -200)/ 100);
        }
        else if (currentValue > 100)
        {
            return Color.Lerp(Constant.color_yellow, Constant.color_green, (currentVal -100)/ 100);
        }

        return Constant.color_yellow;
    }

    private string ConvertValue(float value)
    {

        if (value >= 1000)
        {
            return (value / 1000).ToString("F2") + "k";
        }
        else
        {
            return value.ToString();
        }
    }
}
