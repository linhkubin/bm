﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Target : Enemy
{
    private Transform target;

    public override void Death()
    {
        base.Death();
    }
   
    Quaternion to;
    bool impactObstacle = false;

    public override void Start()
    {
        base.Start();
        target = Player.Instance.GetTransform();
        impactObstacle = false;
    }

    private void Update()
    {
        if (!isDead && LevelManager.Instance.startLevel && move && !Player.Instance.isDead)
        {
            if (!impactObstacle)
            {
                Vector2 direction = target.position - transform.position;
                to = Quaternion.FromToRotation(Vector3.up, direction);
                transform.rotation = Quaternion.Lerp(transform.rotation, to, 3 * Time.deltaTime);
            }

            transform.Translate(Time.deltaTime * transform.up * movementSpeed, Space.World);
        }
        else
        {
            GetComponent<Rigidbody2D>().velocity = Vector2.zero;
            //chong xoay
        }

    }


    public override void ChangeDirection()
    {
        base.ChangeDirection();
    }

    public override void OnTriggerEnter2D(Collider2D other)
    {
        base.OnTriggerEnter2D(other);

        if (inAttackArea)
        {
            if (other.tag == "Ngang")
            {
                impactObstacle = true;
                if (Vector3.Angle(GetTransform().up, Vector3.left) < 90)
                {
                    transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.Euler(new Vector3(0, 0, 90)), Time.time * 15f);

                }
                else
                {
                    transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.Euler(new Vector3(0, 0, 270)), Time.time * 15f);
                }
            }
            if (other.tag == "Doc")
            {
                impactObstacle = true;
                if (Vector3.Angle(GetTransform().up, Vector3.up) < 90)
                {
                    transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.Euler(new Vector3(0, 0, 0)), Time.time * 15f);
                }
                else
                {
                    transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.Euler(new Vector3(0, 0, 180)), Time.time * 15f);
                }
            }
        }

    }

    public override void Active()
    {
        base.Active();
    }

    public override void TakeDamgeBase()
    {
        base.TakeDamgeBase();
    }

    public override void TakeDamgeMain()
    {
        base.TakeDamgeMain();
    }

    public override void TakeEffect()
    {
        base.TakeEffect();
    }

    public override void OnTriggerExit2D(Collider2D collision)
    {
        base.OnTriggerExit2D(collision);
        if (collision.tag == "Ngang" || collision.tag == "Doc")
        {
            impactObstacle = false;
        }
    }
}
