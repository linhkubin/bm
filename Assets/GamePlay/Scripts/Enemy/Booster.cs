﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Booster : Enemy
{
    float angle;
    Quaternion to;
    bool stopMove = false;

    // lay goc ban dau lam xuat phat--> random goc luc dau de xuat phat
    public override void Start()
    {
        base.Start();
        //StartCoroutine(MoveTarget());
        stopMove = false;

        Vector3 target = new Vector3(Random.Range(LevelManager.Instance.areaBot.position.x, LevelManager.Instance.areaTop.position.x), Random.Range(LevelManager.Instance.areaBot.position.y, LevelManager.Instance.areaTop.position.y), 0);

        Vector3 direction = target - GetTransform().position;

        Quaternion to = Quaternion.FromToRotation(Vector3.up, direction);

        GetTransform().rotation = to;

        angle = GetTransform().eulerAngles.z;

        //Debug.Log(" " + angleFrom);
        //Debug.Log(" " + angleTo);
        //Debug.Log(" " + angle);
        //haveKnife = true;
    }

    private void Update()
    {
        if (!isDead && LevelManager.Instance.startLevel && move && !Player.Instance.isDead)
        {
            transform.Translate(Time.deltaTime * transform.up * movementSpeed, Space.World);
            transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.Euler(new Vector3(0, 0, angle)), Time.time * 15f);
        }
        else
        {
            MyRigidbody2D.velocity = Vector2.zero;
            transform.rotation = Quaternion.Euler(new Vector3(0, 0, angle));
        }

    }

    public override void ChangeDirection()
    {
        base.ChangeDirection();
    }

    public override void OnTriggerEnter2D(Collider2D other)
    {
        base.OnTriggerEnter2D(other);

        if (inAttackArea)
        {
            if (other.tag == "Ngang")
            {
                angle += 180;
                float ran = Random.Range(-20, 20);
                angle = -angle + ran;
            }
            if (other.tag == "Doc")
            {
                float ran = Random.Range(-20, 20);
                angle = -angle + ran;
            }
        }

    }

    public override void Active()
    {
        base.Active();
    }

    public override void TakeDamgeBase()
    {
        base.TakeDamgeBase();
    }

    public override void TakeDamgeMain()
    {
        base.TakeDamgeMain();
    }

    public override void TakeEffect()
    {
        base.TakeEffect();
    }

    public override void Death()
    {
        PowerBooster go = SimplePool.Spawn("powerBooster", GetTransform().position, Quaternion.identity).GetComponent<PowerBooster>();
        go.Active();
        base.Death();
    }

    public override void OnTriggerExit2D(Collider2D collision)
    {
        base.OnTriggerExit2D(collision);
    }
}
