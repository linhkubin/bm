﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Enemy : MonoBehaviour
{

    [SerializeField]
    protected float movementSpeed;

    [SerializeField]
    protected Stat health;

    [SerializeField]
    protected int id;

    [SerializeField]
    protected int type;

    //[SerializeField]
    //protected CircleCollider2D collider;

    protected bool inAttackArea = false;

    public bool haveKnife = false;

    //[SerializeField]
    //private List<string> damageSources;

    public bool isDead = false;

    public bool Attack { get; set; }

    public bool TakingDamage { get; set; }

    //public Animator MyAnimator;

    public Rigidbody2D MyRigidbody2D { get; set; }

    protected Transform tf_gameobject;
    public Transform GetTransform()
    {
        if (tf_gameobject == null)
        {
            tf_gameobject = gameObject.transform;
        }
        return tf_gameobject;
    }

    protected bool move = false;

    public WaveEnemy wave;

    private void Awake()
    {
        MyRigidbody2D = GetComponent<Rigidbody2D>();
    }

    // Use this for initialization
    public virtual void Start()
    {
        health.Initialize();
        //collider.isTrigger = true;
        isDead = false;

    }

    public virtual void TakeDamgeBase()
    {
        if (!isDead)
        {
            //MyAnimator.SetTrigger("hurt");

            health.CurrentVal -= Player.Instance.damageBase;

            //Debug.Log("impact base");

            if (health.CurrentVal < 1)
            {
                Death();

            }
        }

    }

    public virtual void TakeDamgeMain()
    {
        if (!isDead)
        {
            //Debug.Log("impact main");

            health.CurrentVal -= Player.Instance.damageMC;

            if (health.CurrentVal < 1)
            {
                Death();
            }
        }
    }

    public virtual void TakeEffect()
    {

    }

    public virtual void Active()
    {
        move = true;
    }

    public virtual void Death()
    {
        //Debug.Log("death");


        //LevelManager.Instance.CheckNumberEnemy();

        if (haveKnife)
        {
            Debug.Log("have knife");
            GameObject go = SimplePool.Spawn("WeaponDrop", GetTransform().position, Quaternion.identity);
            go.GetComponent<StarSpiral>().Drop();
            LevelManager.Instance.listWeapon.Add(go);

            haveKnife = false;
        }

        Reset();
        //thong bao cho wave 
        wave.EnemyDeath();

        SimplePool.Spawn("EnemyDeath", GetTransform().position, Quaternion.identity);

        SimplePool.Despawn(gameObject);

    }

    private void Reset()
    {
        inAttackArea = false;
        health.CurrentVal = 0;
        isDead = true;
        //collider.isTrigger = true;
        move = false;
        haveKnife = false;
    }

    public virtual void ChangeDirection()
    {

    }

    public virtual void OnTriggerEnter2D(Collider2D other)
    {
        if (!inAttackArea && other.tag == "AttackArea")
        {
            inAttackArea = true;
            //collider.isTrigger = false;
        }

        if (other.tag == "BaseHitBox")
        {
            move = false;
            TakeDamgeBase();
            // tao slash va force
            if (!isDead)
            { 
                StartCoroutine(Slash(other.transform));
            }
            //GetTransform().Translate(Time.deltaTime * 2 *(other.transform.right), Space.World);
        }

        if (other.tag == "MainHitBox")
        {
            move = false;
            TakeDamgeMain();
            //MyRigidbody2D.AddForce(GetTransform().position - other.transform.position);
        }

        if (other.tag == "DefeatArea")
        {
            Death(); 
        }
    }

    private IEnumerator Slash(Transform pos)
    {
        for (int i = 0; i < 10; i++)
        {
            GetTransform().Translate(0.25f * Time.deltaTime * (pos.right), Space.World);
        }

        yield return new WaitForSeconds(0.05f);

        SimplePool.Spawn("Slash", pos.position, Quaternion.identity);
        
    }

    public virtual void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "BaseHitBox" || collision.tag == "MainHitBox")
        {
            move = true;
        }
    }

    public void SetWave(WaveEnemy waveEnemy, int id, int type)
    {
        this.id = id;

        this.type = type;

        Reset();

        wave = waveEnemy;

        Start();
    }
}
