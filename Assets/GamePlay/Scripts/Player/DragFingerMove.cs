﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragFingerMove : MonoBehaviour
{
    protected Transform tf_gameobject;
    public Transform GetTransform()
    {
        if (tf_gameobject == null)
        {
            tf_gameobject = gameObject.transform;
        }
        return tf_gameobject;
    }

    private float deltaX, deltaY;
    private Rigidbody2D rigid;
    Vector2 touchPos;
    private bool touchStart = false;

    private bool buttonDown = false;

    private bool active = false;

    private void Start()
    {
        rigid = GetComponent<Rigidbody2D>();
    }

    private void Update()
    {
        if (LevelManager.Instance.startLevel)
        {

            if (!buttonDown)
            {
                if (Input.GetMouseButtonDown(0))
                {
                    active = true;

                    UIManager.Instance.InactiveUIPause();

                    touchPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);

                    deltaX = touchPos.x - transform.position.x;
                    deltaY = touchPos.y - transform.position.y;

                    buttonDown = true;

                }

            }

            if (Input.GetMouseButton(0) && buttonDown)
            {
                touchStart = true;

                //rigid.MovePosition(new Vector2(touchPos.x - deltaX, touchPos.y - deltaY));
            }
            else
            {
                touchStart = false;
            }

            if (Input.GetMouseButtonUp(0) && active)
            {
                buttonDown = false;
                UIManager.Instance.ActiveUIPause();
            }
        }
        else
        {
            touchStart = false;
        }
    }

    private void FixedUpdate()
    {
        if (touchStart)
        {
            touchPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);

            rigid.MovePosition(new Vector2(touchPos.x - deltaX, touchPos.y - deltaY));
            //rigid.MovePosition(new Vector2(Mathf.Clamp(touchPos.x - deltaX, min.x, max.x), Mathf.Clamp(touchPos.y - deltaY, min.y, max.y)));

        }
        else
        {
            rigid.velocity = Vector2.zero;
        }

    }

    public void ResetMove()
    {
        buttonDown = false;
        touchStart = false;
        active = false;
    }

}

