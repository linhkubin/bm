﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StarSpiral : MonoBehaviour
{
    float angle = 15;

    float circleSpeed = 2;
    //float forwardSpeed = -4; // Assuming negative Z is towards the camera
    float circleSize = 0.1f;
    float circleGrowSpeed = 0.02f;

    Animator anim;

    public Transform target;
    private SpriteRenderer s_target;
    public SpriteRenderer s_weapon;
    float f_angle;
    public bool merger;

    private bool canTakeWeapon = false;

    private Transform tf_transform;
    private Transform GetTransform()
    {
        if (tf_transform == null )
        {
            tf_transform = transform;
        }
        return tf_transform;
    }

    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (merger)
        {
            if (!toPlayer)
            {
                GetTransform().Rotate(new Vector3(0, 0, Time.deltaTime * - 500f));
            }
            else if (target != null)
                GetTransform().rotation = Quaternion.RotateTowards(GetTransform().rotation, target.rotation, Time.deltaTime * 500f);

            var xPos = Mathf.Sin(Time.deltaTime * 0.5f * circleSpeed + f_angle) * circleSize + target.position.x;
            var yPos = Mathf.Cos(Time.deltaTime * 0.5f * circleSpeed + f_angle) * circleSize + target.position.y;

            //Debug.Log("transform.localPosition__ " + transform.localPosition + "_timer : " + timer + "_circleSpeed : " + circleSpeed + "_f_angle : " + f_angle + "_circleSize : " + circleSize + "_target.position.x : " + target.position.x);

            GetTransform().position = new Vector3(xPos, yPos, 0);

            if (circleSize < 0.1f && !active)
            {
                if (!toPlayer)
                {
                    merger = false;
                    toPlayer = true;
                    StartCoroutine(Caculate(numWeapon));
                }
                else
                    StartCoroutine(MergerAttack());
            }

            if (circleSize > 0.01f)
            {
                circleSize -= Time.deltaTime;
            }

        }

        ////Debug.Log(Vector2.Distance(Vector2.zero,gameObject.transform.position ));
    }

    bool toPlayer = false;

    bool active = false;

    private IEnumerator MergerAttack()
    {
        if (!active)
        {
            active = true;
            anim.SetTrigger("merger");
            yield return new WaitForSeconds(0.5f);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "BaseRange" && !merger && canTakeWeapon)
        {
            StartCoroutine(CaculateMovePlayer(Player.Instance.AddNewWeapon()));
        }
    }

    int numWeapon = 0;
    private IEnumerator CaculateMovePlayer(int numBaseWeapon)
    {
        if (numBaseWeapon > 0)
        {
            numWeapon = numBaseWeapon;

            toPlayer = false;

            yield return new WaitForSeconds(0.1f);

            active = false;

            target = Player.Instance.transform;

            circleSize = Vector2.Distance(target.position, GetTransform().position);

            f_angle = Vector2.Angle(Vector2.up, GetTransform().position - target.position);

            if (Vector2.Angle(Vector2.left, GetTransform().position - target.position) < 90f)
            {
                f_angle = 360 - f_angle;
            }
            f_angle = f_angle * Mathf.Deg2Rad;

            merger = true;

        }

    }

    private IEnumerator Caculate(int numBaseWeapon)
    {
        if (numBaseWeapon > 0)
        {
            yield return new WaitForSeconds(0.1f);

            active = false;

            //Debug.Log("number : " + numBaseWeapon);

            s_target = Player.Instance.baseWeapon[numBaseWeapon];

            target = Player.Instance.tf_baseWeapon[numBaseWeapon];

            circleSize = Vector2.Distance(target.position, GetTransform().position);

            f_angle = Vector2.Angle(Vector2.up, GetTransform().position - target.position);

            if (Vector2.Angle(Vector2.left, GetTransform().position - target.position) < 90f)
            {
                f_angle = 360 - f_angle;
            }
            f_angle = f_angle * Mathf.Deg2Rad;

            merger = true;
        }
        
    }

    public void ActiveWeapon()
    {
        s_target.enabled = true;
    }

    public void DespawnWeapon()
    {
        SimplePool.Despawn(gameObject);
    }

    public void MergerWeapon()
    {
        StartCoroutine(CaculateMovePlayer(Player.Instance.AddNewWeapon()));
    }

    public void Drop()
    {
        Reset();

        StartCoroutine(DropWeapon());

    }

    private IEnumerator DropWeapon()
    {
        canTakeWeapon = false;

        int range = UnityEngine.Random.Range(20, 32);

        for (int i = 0; i < range; i++)
        {
            yield return new WaitForSeconds(0.012f);
            GetTransform().Translate(Time.deltaTime * (GetTransform().position - Player.Instance.GetTransform().position), Space.World);
            GetTransform().Rotate(new Vector3(0, 0, -i * 1.2f));
        }

        canTakeWeapon = true;
    }

    private void Reset()
    {
        target = null;

        s_weapon.sprite = MCManager.Instance.GetSpriteWeapon();

        numWeapon = 0;

        toPlayer = false;

        active = false;

        merger = false;

        circleSpeed = 2;

        circleSize = 0.1f;
    }

}
