﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour
{
    private static Player instance;
    public static Player Instance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<Player>();
            }
            return instance;
        }
    }

    public bool isDead = false;


    private Transform tfPlayer;
    public Transform GetTransform()
    {
        if (tfPlayer == null)
        {
            tfPlayer = transform;
        }
        return tfPlayer;
    }

    [Header("Weapon")]

    [SerializeField]
    private RectTransform baseAttack;

    [SerializeField]
    private RectTransform mainAttack;

    public SpriteRenderer[] baseWeapon;

    public Transform[] tf_baseWeapon;

    public int numBaseWeapon = 6;

    public int damageBase = 15;

    public int damageMC = 20;

    [SerializeField]
    private SpriteRenderer[] mainWeapon;

    [Header("Move")]

    public DragFingerMove moveType;

    public GameObject[] weapon;

    [Header("Anim")]

    public Animator anim;

    private void Awake()
    {
        Time.fixedDeltaTime = 0.02f;
        //->fix fps = 60
    }

    public int AddNewWeapon()
    {
        if (numBaseWeapon < 15)
        {
            tf_baseWeapon[numBaseWeapon].gameObject.SetActive(true);
            return numBaseWeapon++;
        }
        else
            return 0;
        //go.GetComponent<StarSpiral>().SetTarget(go);

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Enemy")
        {
            if (!isDead)
            {
                isDead = true;
                LevelManager.Instance.FinishLevel(false);
                PlayerEnableMove(false);
                PlayerState("die");
                //StartCoroutine(GameOver());
            }
        }
    }

    //private void OnCollisionEnter2D(Collision2D collision)
    //{
    //    if (collision.gameObject.tag == "Enemy")
    //    {
    //        if (!isDead)
    //        {
    //            LevelManager.Instance.FinishLevel(false);
    //            move.ResetMove();
    //            PlayerActive(false);
    //            //StartCoroutine(GameOver());
    //        }
    //    }
    //}

    public void PlayerActive(bool active)
    {

        //reset
        weapon[0].SetActive(active);
        weapon[1].SetActive(active);
        isDead = !active;

    }


    public void ResetWeapon()
    {
        for (int i = 6; i < 15; i++)
        {
            baseWeapon[i].enabled = false;
            tf_baseWeapon[i].gameObject.SetActive(false);
        }

        numBaseWeapon = 6;
    }

    #region Anim

    public void PlayerState(string state)
    {
        /*
         - start
         - reset
         - die
         - win
         - revive
         */
        anim.SetTrigger(state);
    }


    //active UI win
    public void UILevelWin()
    {
        StartCoroutine(UIMainScene.Instance.LevelWin());
    }

    public void MoveNextLevel()
    {
        StartCoroutine(MoveZero());
    }

    private IEnumerator MoveZero()
    {
        yield return new WaitForSeconds(0.5f);
        for (int i = 0; i < 30; i++)
        {
            yield return new WaitForSeconds(0.01f);
            GetTransform().position = Vector3.MoveTowards(GetTransform().position, new Vector3(0, GetTransform().position.y), 5*Time.deltaTime);
        }
    }
    #endregion

    #region Move 
    public void PlayerEnableMove(bool move)
    {
        moveType.enabled = move;
    }

    public void PlayerStartMove()
    {
        moveType.enabled = true;
    }
    #endregion
}
