﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wall : MonoBehaviour
{

    public Transform tf_gameobject;

    public Transform max, min;

    private float maxX,maxY;
    private float minX,minY;

    // Start is called before the first frame update
    void Start()
    {
        maxX = max.position.x ;
        maxY = max.position.y ;
        minX = min.position.x ;
        minY = min.position.y ;
    }

    public int Check(Vector3 position)
    {

        if ( position.y < maxY && position.y > minY)
        {

            if (tf_gameobject.position.x < position.x)
            {
                return 1;//right
            }
            else
            {
                return 2;//left
            }
        }

        if (position.x < maxX && position.x > minX)
        {
            if (tf_gameobject.position.y < position.y)
            {
                return 3;//upper
            }
            else
            {
                return 4;//lower
            }
        }

        return 0;
    }
}
