﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateBase : MonoBehaviour
{
    public int speed;

    private Transform tf_spin;
    private Transform GetTransform()
    {
        if (tf_spin == null)
        {
            tf_spin = transform;
        }

        return tf_spin;
    }

    // Update is called once per frame
    void Update()
    {
        GetTransform().Rotate(new Vector3(0, 0, -Time.deltaTime * speed));
    }
}
