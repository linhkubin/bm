﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestMove : MonoBehaviour
{
    Transform tf_gameobject;
    public Transform GetTransform()
    {
        if (tf_gameobject == null)
        {
            tf_gameobject = gameObject.transform;
        }
        return tf_gameobject;
    }

    float angle;

    // Start is called before the first frame update
    void Start()
    {
        //Vector3 target = new Vector3(Random.Range(LevelManager.Instance.areaBot.position.x, LevelManager.Instance.areaTop.position.x), Random.Range(LevelManager.Instance.areaBot.position.y, LevelManager.Instance.areaTop.position.y), 0);

        //Vector3 direction = target - GetTransform().position;

        //Quaternion to = Quaternion.FromToRotation(Vector3.up, direction);

        //GetTransform().rotation = to;

        angle = GetTransform().eulerAngles.z;

    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(Time.deltaTime * transform.up * 2, Space.World);
        transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.Euler(new Vector3(0, 0, angle)), Time.time * 15f);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        //if (collision.tag == "Ngang")
        //{
        //    angle += 180;
        //    float ran = Random.Range(-20, 20);
        //    angle = -angle + ran;
        //}
        //if (collision.tag == "Doc")
        //{
        //    float ran = Random.Range(-20, 20);
        //    angle = -angle + ran;
        //}

        if (collision.tag == "Ngang")
        {
            if (Vector3.Angle(GetTransform().up, Vector3.left) < 90)
            {
                angle = 90;
            }
            else
            {
                angle = 270;
            }
        }
        if (collision.tag == "Doc")
        {
            if (Vector3.Angle(GetTransform().up, Vector3.up) < 90)
            {
                angle = 0;
            }
            else
            {
                angle = 180;
            }
        }
    }
}
